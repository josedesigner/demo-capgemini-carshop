import service from '@server/service/AuthService';
import { Router } from 'express';
import userModel, { User } from '@server/domain/user/userModel';
import jwt from 'jsonwebtoken';
import {Types} from 'mongoose'

let SECRET="secret";
const routes = Router();
// rota para login de um usuário. Gera um token que é usado nos headers de todos os requests posteriores!
routes.post('/login', isValidUser, async (req, res) => {
    const loggedUser: User | null = await service.login(req.body);
    if (loggedUser === null) {
        res.status(401).json({ message: "Authentication failed: Username or password is incorrect." });
    }
    else {
        const token=jwt.sign({username:loggedUser.username},SECRET,{expiresIn: 600})
        res.status(200).json({user:loggedUser,token:token, auth:true});
    };
})

//Rota para criar usuário com senha encriptada. Verifica se o nome de usuário já existe. Se existe ele não cria:)
routes.post('/signIn', isValidUser, async (req, res) => {
    const signedUser: User | null = await service.signIn(req.body);
    if (signedUser == null) {
        res.status(401).json({ message: "user name already exist" });
    }
    else{
        res.status(201).json({message: "user created with success"});
    }

})

//middleware para verificar se são passados no body do request os dois parâmetros( username e password)
function isValidUser(req: any, res: any, next: any) {
    const user = req.body;
    if ((user.username == null) || (user.password == null)) {
        return res.status(401).send({ message: "Username or password is missing." });
    }
    next();
}


export default routes;
export { routes }