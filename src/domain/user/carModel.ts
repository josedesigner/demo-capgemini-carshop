import {Schema, model, Types} from 'mongoose';
import CarItem from './carItem';

export interface Car {
    id: Types.ObjectId;
    products: Array<CarItem>;
    owner:string;
    paymentMethod:number;
}

const UserSchema = new Schema<Car>({
  id:Types.ObjectId,
  products:Array,
  owner:String,
  paymentMethod:Number
});

export default model<Car>("Car", UserSchema);