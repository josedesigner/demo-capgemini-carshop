import userModel, { User } from '@server/domain/user/userModel';


class UserService {
    async listUsers(): Promise<Array<User>> {
        const list=await userModel.find();
        return list;
    }
    async saveUser(params: Omit<User, 'id'>): Promise<User> {
        return await new userModel(params).save();
    }
    async listOneUser(id: String): Promise<User | null> {
        const selectedUser = await userModel.findById(id);
        
        if (selectedUser === null) {
            return null;
        }
        return selectedUser;
    }
    async deleteUser (id:String):Promise<object>{
        try {
            await userModel.deleteOne({_id:id})
            return {message:"object deleted"}
        } catch (error) {
            return {error:error};
        }
    }
 
}

export default new UserService();