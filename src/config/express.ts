import express from "express";
import routes from "./routes";
import session from "express-session";
declare module 'express-session' {
  export interface SessionData {
    car: { [key: string]: any };
  }
}
const createServer = (): express.Application => {
  const app = express();

  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());
  app.use(
    session({
      secret: "mysecret",
      cookie: { maxAge: 180 * 60 * 1000 },
    })
  );
  app.use("/", routes);

  app.disable("x-powered-by");

  app.get("/health", (_req, res) => {
    res.send("UP");
  });

  return app;
};
export { createServer, session };
