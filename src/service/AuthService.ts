import userModel, { User } from "@server/domain/user/userModel";
import bcrypt from "bcrypt";
class AuthService {
  async login(body: User): Promise<User | null> {
    const logger: User | null = await userModel
      .findOne({ username: body.username })
      .exec();

    if (logger === null) {
      return null;
    }
    if (await bcrypt.compare(body.password, logger.password)) {
      return logger;
    }
    return null;
  }

  async signIn(body: User): Promise<User | null> {
    const logger: User | null = await userModel
      .findOne({ username: body.username })
      .exec();
    if (logger !== null) {
      return null;
    }
    const salt = await bcrypt.genSalt(12);
    const hashPassword = await bcrypt.hash(body.password, salt);
    const newUser = new userModel({
      username: body.username,
      password: hashPassword,
    }).save();
    return newUser;
  }
}

export default new AuthService();
