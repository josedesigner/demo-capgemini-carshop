import {Schema, model, Types} from 'mongoose';

export interface User {
    id: string;
    username: string;
    password: string;
}

const UserSchema = new Schema<User>({
    id:String,
    username: {type:String,required:true},
    password:String
});

export default model<User>("User", UserSchema);
