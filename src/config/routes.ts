import {Router} from 'express';
import {routes as userRoutes} from '@controller/UserController';
import {routes as productRouter} from '@controller/ProductConroller'
import {routes as loginRouter} from '@controller/LoginController'
import {routes as carRouter} from '@controller/CarController'

const routes = Router();
routes.use("/user", userRoutes);
routes.use("/product",productRouter);
routes.use("/",loginRouter);
routes.use("/car",carRouter)
export default routes;
