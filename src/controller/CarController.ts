import service from "@server/service/CarService";
import { Router } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import { session } from "@config/express";
import CarItem from "@server/domain/user/carItem";
import { Product } from "@domain/user/productModel";
import carModel, {Car} from "@server/domain/user/carModel";


const routes = Router();

routes.get("/:id", async (req, res) => {
  const productID = req.params.id;
  const product: Product | null = await service.getProduct(productID);
  if(!product){
    return res.status(400).json({message:'product does not exist'});
  }
  if(!req.session.car){
    const car = new Array();
    const item= new CarItem(productID,product.name);
    car.push(item);
    req.session.car=car;
    return res.status(201).json({message:'car created',car: req.session.car});
  }
  const shopingCar=req.session.car;
  let item:CarItem|null=null;

  shopingCar.forEach((prod:any) => {
    if (prod.name == product.name) {
      item=prod;
      return;
     }
  })
  if(item){
    
    const index = shopingCar.indexOf(item);
    if(shopingCar[index].quantity<product.quantity){
    shopingCar[index].quantity++;
    req.session.car=shopingCar;
    return res.status(200).json({message:'car updated',car: req.session.car});
    }
    return res.status(400).json({message:'you cant buy this product anymore'})
  }
  item= new CarItem(productID,product.name);
  shopingCar.push(item);
  req.session.car=shopingCar;
  return res.status(201).json({message:'product added',car: req.session.car});

});

routes.delete("/:id", async (req, res) => {
  const productID = req.params.id;
  const product: Product | null = await service.getProduct(productID);
  if(!product){
    return res.status(400).json({message:'product does not exist'});
  }
  if(!req.session.car){
    return res.status(400).json({message:'you dont have a shoping car'});
  }
  const shopingCar=req.session.car;
  let item:CarItem|null=null;

  shopingCar.forEach((prod:any) => {
    if (prod.name == product.name) {
      item=prod;
      return;
     }
  })
  if(item){
    
    const index = shopingCar.indexOf(item);
    console.log(shopingCar[index].quantity)
    
    if(shopingCar[index].quantity>1){
    shopingCar[index].quantity--;
    
    req.session.car=shopingCar;
    return res.status(200).json({message:'car updated',car: req.session.car});
    }
    return res.status(400).json({message:'you have eliminated the product'})
  }
  
  return res.status(201).json({message:'you dont have this product in your car'});
        
});

routes.post("/", async (req, res) => {
  const listOfProducts= req.session.car;

  if(req.session.car && req.body){

  const car:any={

  products:listOfProducts,
  owner:req.body.name,
  paymentMethod:req.body.paymentMethod
  }
  service.finishPurshase(car);
  return res.status(201).json({message:'purshase made',car:car})
}

return res.status(400).json('You did not make a purshase')

});




export default routes;
export { routes };
