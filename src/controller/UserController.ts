import service from '@service/UserService';
import { Router } from 'express';
import jwt from 'jsonwebtoken';

let SECRET="secret";
const routes = Router();

routes.get("/", async (req, res) => {
    res.json(await service.listUsers());
})
routes.get('/:id',authJWT, async (req, res) => {
    return res.json(await service.listOneUser(req.params.id));

})
routes.delete('/:id',authJWT, async (req, res) => {
    return res.json(await service.deleteUser(req.params.id))
})

function authJWT(req:any, res:any, next:any){
    const token = req.headers['x-access-token'];
    try {
        const username=jwt.verify(token,SECRET);
        req.username=username;
        next();
    } catch (error) {
        return res.status(401).json({message:"you are unauthorized to make this change"});
    } 
    

}

export {authJWT as autentication, SECRET as secret}
export default routes;
export { routes }