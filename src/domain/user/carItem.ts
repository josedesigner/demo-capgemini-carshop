class CarItem {
  private id: string;
  private quantity: number;
  private name: string;

  constructor(id: string, name: string) {
    this.id = id;
    this.quantity=1;
    this.name = name;
  }

}

export default CarItem;
