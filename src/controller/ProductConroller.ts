import service from '@server/service/ProductService';
import { Router } from 'express';
import { ObjectId, Types } from 'mongoose';
import { autentication } from './UserController';


const routes = Router();
routes.get("/", async (req, res) => {
  const list = await service.listProducts();
  if (list.length === 0) {
    return res.status(200).json({ message: "The store is empty. There are no products available"! })
  }
  return res.status(200).json(await service.listProducts());

})
routes.get("/:id", async (req, res) => {
  try {
    const brandProducts= await service.listProductByBrand(req.params.id);
    if(brandProducts===null){
      return res.status(400).json({message:"Products of that brand are unavailable"})
    }
    return res.status(200).json(brandProducts);
  } catch (error) {
    return res.status(401).json({
      message: "You have no authorization to see this data"
    })
  }
})
routes.post("/",autentication, async (req, res) => {
  try {
    return res.status(201).json(await service.saveProduct(req.body));
  } catch (error) {
    return res.status(400).json({
      message: "Please add a valid product. One or more fields are missing",
      observation: "The product must have a name, a brand, a quantity and a price"
    })
  }
})

routes.put('/:id',autentication, async (req, res) => {
  try {
    const productUpdated = await service.updateProduct(req.body.price, req.params.id);
    if (productUpdated === null) {
      return res.status(400).json({ message: "product not found" })
    }
    return res.status(200).json({ product: productUpdated, status: "updated" });
  } catch (error) {
    return res.json(401).json({ message: "You have no permission to update this product" })
  }
})

routes.delete('/:id',autentication, async (req, res) => {
  try {
    const id: String = req.params.id;
    const productDelected = await service.deleteProduct(id);
    if (productDelected === null) {
      return res.status(400).json({ message: "the product does not exist" })
    }
    return res.status(200).json({ message: "product delected" })
  } catch (error) {
    return res.status(401).json({ message: "You don't have permission to delete the product" })
  }
})


export default routes;
export { routes }




