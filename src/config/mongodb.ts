import mongoose from 'mongoose';
import logger from './logger';

const {
    MONGODB_HOST,  
} = process.env

async function connect(){   
    logger.info("connecting to mongodb at "+ MONGODB_HOST);
    const uri = `mongodb://localhost:27017/carshop`;
    return await mongoose.connect(uri)
    .catch(logger.info)
}

export default connect;

