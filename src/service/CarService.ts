import carModel, { Car } from '@server/domain/user/carModel';
import productModel from '@server/domain/user/productModel';

class CarService {
    async getProduct(id:string) {    
      let product;
      try {
        product=await productModel.findById(id);

      } catch (error) {
        
        product=null;
      }
      return product;
    }
    async finishPurshase(params: Car): Promise<Car> {
      return await new carModel(params).save();
      
    }
}


export default new CarService();