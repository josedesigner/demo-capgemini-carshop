import { Schema, model, Types } from 'mongoose';

export interface Product {
  name: string;
  description: string;
  quantity: number;
  price: number;
}

const ProductSchema = new Schema<Product>({
  name: {type:String,required:true},
  description: {type:String,required:true},
  quantity:{type:Number,required:true},
  price: {type:Number,required:true}
});

export default model<Product>("Product", ProductSchema);
