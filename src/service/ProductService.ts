import productModel, { Product } from '@server/domain/user/productModel';

class ProductService {
    async listProducts(): Promise<Array<Product>> {
        return await productModel.find();
    }
    async listProductByBrand(brand: string) {
        const products =await productModel.find({ description: brand }).exec();
        if(products.length===0){
            return null;
        }
        return products;

    }
    async saveProduct(params: Product): Promise<Product> {
        return await new productModel(params).save();
    }
    async deleteProduct(id: String) {
        try {
            //ele não está a retornar null (problema de casting)...resolvi com try and catch
            const product: Product | null = await productModel.findById(id).exec();
            return await productModel.deleteOne({ _id: id })
        } catch (error) {
            return null;
        }
    }
    async updateProduct(info: number, id: String) {
        try {
            const product: Product | null = await productModel.findById(id).exec();
            if (product === null) {
                return null;
            }
            product.price = info;
            await new productModel(product).save();
            return product;
        } catch (error) {
            return null;
        }
    }

}


export default new ProductService();